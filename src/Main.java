import javax.swing.*;

/*
Klasa Main odpowiada za wczytanie danych z klawiatury i stworzenie instancji klasy MainFrame,
w ktorej rysowane beda sinusoidy.
 */
public class Main {
    public static void main(String[] args) {
        int amplitude1, amplitude2, frequency1, frequency2;

        JFrame frame = new JFrame();

        /*
        Uzytkownik wpisuje dane dotyczace sinusoid: amplitudy i czestotliwosci
         */
        String amp1 = JOptionPane.showInputDialog(frame, "Amplituda pierwszej sinusoidy: ");
        amplitude1 = Integer.parseInt(amp1);

        String freq1 = JOptionPane.showInputDialog(frame, "Czestotliwosc pierwszej sinusoidy: ");
        frequency1 = Integer.parseInt(freq1);

        String amp2 = JOptionPane.showInputDialog(frame, "Amplituda drugiej sinusoidy: ");
        amplitude2 = Integer.parseInt(amp2);

        String freq2 = JOptionPane.showInputDialog(frame, "Czestotliwosc drugiej sinusoidy: ");
        frequency2 = Integer.parseInt(freq2);

        frame.dispose();

        MainFrame sinFrame = new MainFrame(amplitude1, frequency1, amplitude2, frequency2);
    }
}

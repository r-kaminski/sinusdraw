import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class MainFrame extends JFrame {

    private SinusPlot firstPanel;
    private SinusPlot secondPanel;
    private SinusPlot thirdPanel;
    private ArrayList<SinusPlot> panels;

    private int amplitude1, frequency1, amplitude2, frequency2;

    public MainFrame(int amplitude1,int frequency1, int amplitude2, int frequency2) {

        this.amplitude1 = amplitude1;
        this.frequency1 = frequency1;
        this.amplitude2 = amplitude2;
        this.frequency2 = frequency2;

        setSize(400, 640);
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JPanel contentPane = new JPanel();
        setContentPane(contentPane);
        contentPane.setLayout(null);

        firstPanel = new SinusPlot();
        firstPanel.setAmplitude(amplitude1);
        firstPanel.setFrequency(frequency1);


        secondPanel = new SinusPlot();
        secondPanel.setAmplitude(amplitude2);
        secondPanel.setFrequency(frequency2);

        thirdPanel = new SinusPlot();
        thirdPanel.setAmplitude(amplitude1+amplitude2);
        thirdPanel.setFrequency(frequency1+frequency2);

        firstPanel.setBounds(25, 10, 350, 150);
        secondPanel.setBounds(25, 170, 350, 150);
        thirdPanel.setBounds(25, 330, 350, 150);

        panels = new ArrayList<>();
        panels.add(firstPanel);
        panels.add(secondPanel);
        panels.add(thirdPanel);

        for (SinusPlot panel : panels) {
            contentPane.add(panel);
        }

        JButton start = new JButton("Start");
        start.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                for (SinusPlot panel : panels) {
                    panel.timer.restart();
                }

            }
        });
        start.setBounds(165, 505, 70, 30);
        contentPane.add(start);


        JButton stop = new JButton("Stop");
        stop.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(firstPanel.timer.isRunning()) {
                    for (SinusPlot panel : panels) {
                        panel.timer.stop();
                    }
                }
            }
        });
        stop.setBounds(165, 555, 70, 30);
        contentPane.add(stop);

        setVisible(true);
    }
}

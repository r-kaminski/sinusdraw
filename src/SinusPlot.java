
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SinusPlot extends JPanel {
    private int amplitude;
    private int frequency;

    private int phase = 0;
    public Timer timer;

    public SinusPlot() {
        timer = new Timer(60, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                repaint();
                phase++;
                if(phase >= 360) {
                    phase = 0;
                }
            }
        });
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawLine(0, getHeight()/2, getWidth(), getHeight()/2);
        plotSin(g);
    }


    private void plotSin(Graphics g) {
        Polygon line = new Polygon();
        for(int x = 0; x <= getWidth(); x++) {
            double y = (amplitude * Math.sin((Math.PI/180)*2*frequency*x + phase + 1)) + getHeight()/2;
            line.addPoint(x, (int)y);
        }

        Graphics2D g2d = (Graphics2D) g;
        g2d.setColor(Color.BLACK);
        g2d.drawPolyline(line.xpoints, line.ypoints, line.npoints);

    }

    public void setAmplitude(int amplitude) {
        this.amplitude = amplitude;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

}
